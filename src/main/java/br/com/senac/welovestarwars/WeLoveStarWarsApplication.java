package br.com.senac.welovestarwars;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Component;

@SpringBootApplication
public class WeLoveStarWarsApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(WeLoveStarWarsApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

	}
	

}
