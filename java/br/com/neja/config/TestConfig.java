package br.com.senac.welovestarwars.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import br.com.senac.welovestarwars.services.DBService;
import br.com.senac.welovestarwars.services.mail.EmailService;
import br.com.senac.welovestarwars.services.mail.MockEmailService;

@Configuration
@Profile("test")
public class TestConfig {
	@Autowired
	private DBService dbService;

	@Bean
	public boolean initDatabase() {

		dbService.initTestDatabase();
		return true;
	}

	@Bean
	public EmailService emailService() {
		return new MockEmailService();
	}

}
