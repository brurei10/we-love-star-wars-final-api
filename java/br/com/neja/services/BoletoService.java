package br.com.senac.welovestarwars.services;

import java.time.LocalDateTime;

import org.springframework.stereotype.Service;

import br.com.senac.welovestarwars.domain.PagamentoComBoleto;

@Service
public class BoletoService {

	public void preencherPagamentoBoleto(PagamentoComBoleto pgto, LocalDateTime instante) {
		pgto.setDataVencimento(instante.plusDays(7));
	}
	
	
	
}
