
# We love Star Wars, API

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

[![Build Status](https://drive.google.com/file/d/16In6zKKjOSzujdUyEMZaeRkJv6_5wxJk/view?usp=sharing)](https://travis-ci.org/joemccann/dillinger)


 Um estudo de caso de uma aplicação de solicita��o de filmes utilizando as plataformas <b>Spring</b> e <b>Ionic</b>.
 No Back-end eu utilizei da grande variedade do ecosistema Spring como o
 <b>Spring Data JPA </b> para a persistência de dados.<br>
<b>Spring Security</b> com <b>JWT</b> para a parte de segurança da aplicação.<br>
<b>Spring Boot</b> para facilitar a configuração.<br>
<b>Amazon S3</b> para armazenar as fotos.<br>

# New Features!

  - Esqueleto padrao para funcionamento do servico;
  - Estrutura generica para internacionalizacao;
  - Controle de mensagens customizadas dos retornos https
  - Centralizacao dos servicos gerados;
  - Modo de desenvolvimendo com banco H2 embarcado;

### Tech

Para este projeto, foram utilizadas as seguintes tecnologias:

* [JAVA] - Kernel de execucao do servico e linguagem principal.
* [Eclipse ou Intellj] - IDEs para desenvolvimento da aplicacao.
* [Maven] - Para gestao das dependencias e processos de build.
* [Spring Boot] - Framework java para execucao de IOC, MVC, REST, DATA, SERVICES
* [H2] - Banco embarcado para modo desenvolvimento
* [Tomcat] - Web server para deploy dos servicos criados
* [JPA] - API para controle de transacoes da DATABASE
* [JENKINS] -  Para gestao e automatizacao do processo CI/CD
* [BITBUCKET] - SCM para gestao dos fontes criados

### Installation

- Deve-se baixar o projeto do repositorio;
- Deve-se Instalar o java na maquina;
- Deve-se converter o projeto para estrutrua maven;
- Deve-se efetuar as seguintes etapas de build:

```
$ mvn clean
$ mvn install
$ run as spring boot app
```

For production environments...

```sh
$ Em construcao...
```

### Development

Segue abaixo as principais estruturas do projeto

br.com.senac:
```
$ Pacote contendo a classe main de execucao do projeto
```

br.com.senac.welovestarwars.application.api:
```
$ Pacote generico para parametrizacao e utilizacao das Apis do projeto
``` 

br.com.senac.welovestarwars.application.assembler:
```
$ Pacote que extend o RepresentationModelAssemblerSupport criando modelos representativos e funcionais
```

br.com.senac.welovestarwars.application.converter:
```
$ Pacote contendo as conversoes dos objetos do projeto com os objetos representativos
```

br.com.senac.domain:
```
$ Modelos do projeto (Objetos persistentes)
```

br.com.senac.request:
```
$ Pacote com os interceptors das requisicoes do projeto
```

br.com.senac.response:
```
$ Pacote com os interceptors dos retornos das requests do projeto
```

br.com.senac.welovestarwars.domain.enums:
```
$ Lista de atributos constantes enumerados do projeto
```

br.com.senac.welovestarwars.application.exceptions:
```
$ Contem todo o controle de excessao do projeto
```

br.com.senac.welovestarwars.application.i18n:
```
$ Pacote responsavel pela internacionalizacao
```

br.com.senac.welovestarwars.infrastructure.repository:
```
$ Pacoteb responsavel por efetuar a conexao entre a persistencia de dados. Tambem contem os metodos padroes do CrudRepository e JpaRepository
```

br.com.senac.resources:
```
$ Recuros utilizados no projeto (servicos ou endpoints)
```

br.com.senac.welovestarwars.application.rest:
```
$ Responsavel por conter o mapeamento e controle das transacoes rest como swagger, rest api, http etc
```

br.com.senac.welovestarwars.domain.services:
```
$ Camada de servicos.
```

br.com.senac.utils:
```
$ Pacote para utilitarios do projeto
```



